package com.javaproject.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CouponServiceJavaProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(CouponServiceJavaProjectApplication.class, args);
	}

}
