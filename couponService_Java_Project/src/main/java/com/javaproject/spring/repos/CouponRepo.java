package com.javaproject.spring.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.javaproject.spring.model.Coupon;

public interface CouponRepo extends JpaRepository<Coupon, Long> {
	
	Coupon findByCode(String code);

}
