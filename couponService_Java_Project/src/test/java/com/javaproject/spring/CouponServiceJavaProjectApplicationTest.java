package com.javaproject.spring;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;

import com.javaproject.spring.controller.CouponRestController;
import com.javaproject.spring.model.Coupon;
import com.javaproject.spring.repos.CouponRepo;

@SpringBootTest
public class CouponServiceJavaProjectApplicationTest {
	
	private static final String SUPERSALE = "SUPERSALE";
	
	@Mock
	private CouponRepo repo;
	
	@InjectMocks
	private CouponRestController controller;
	

	@Test
	void testCreate() {
		
		Coupon coupon = new Coupon();
		when(repo.save(coupon)).thenReturn(coupon);
		Coupon couponCreated = controller.create(coupon);
		verify(repo).save(coupon);
		
	}
	
	@Test
	void testGetCoupon() {
		
		Coupon coupon = new Coupon();
		coupon.setId(12l);
		coupon.setCode(SUPERSALE);
		coupon.setDiscount(new BigDecimal(10));
		when(repo.findByCode(SUPERSALE)).thenReturn(coupon);
		Coupon couponResponse = controller.getCoupon(SUPERSALE);
		verify(repo).findByCode(SUPERSALE);
		
	}

}
